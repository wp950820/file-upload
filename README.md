<!--
 * @Author: your name
 * @Date: 2020-09-10 17:24:02
 * @LastEditTime: 2020-09-10 17:30:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \file-upload-masterc:\Users\admin\Desktop\study\big-file-upload\README.md
-->
# 大文件上传

#### 介绍
前端大文件上传 + 断点续传解决方案，重新演示上传需要删除 /target 中的文件，否则由于服务端保存了文件上传会直接成功。
前端
* vue + element 界面展示
* Blob#slice 实现文件切片
* FileReader + spark-md5 + web-worker 生成文件 hash
* xhr 发送 formData

服务端
* nodejs
* multiparty 处理 formData

